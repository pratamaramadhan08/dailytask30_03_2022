// 1
class Vehicle {
    constructor(jenis_kendaraan_roda, negara_produksi){
        this.jenis_kendaraan_roda = jenis_kendaraan_roda;
        this.negara_produksi = negara_produksi;
    }

    infoKendaraan(){
        console.log(`Jenis Kendaraan ${this.jenis_kendaraan_roda} dari ${this.negara_produksi}`)
    }
}


//2

class Mobil extends Vehicle {
    constructor(jenis_kendaraan_roda, negara_produksi, merek_kendaraan, harga_kendaraan, persen_pajak){
        super(jenis_kendaraan_roda, negara_produksi)
        this.merek_kendaraan = merek_kendaraan;
        this.harga_kendaraan = harga_kendaraan;
        this.persen_pajak = persen_pajak;   
        this.totalHarga = this.harga_kendaraan + this.persen_pajak;
    }
    // 3
    totalPrice(){
        console.log(`Total harga Rp. ${this.totalHarga}`);
    }
    infoKendaraan(){
        console.log(`Kendaraan ini mereknya ${this.merek_kendaraan} dengan total harga Rp${this.totalHarga}`);
    }
}
//1
const kendaraan1 = new Vehicle('Roda 4', 'Jerman');
kendaraan1.infoKendaraan()

const kendaraanMobil = new Mobil('Roda 4', 'Belanda', 'Mercedes', 50000000, 1000);
kendaraanMobil.infoKendaraan()


